*This script processes and merges the files in /Build/Input into the main dataset in /Build/Output

clear
set more off

capture cd "C:\Users\awjia\Dropbox\Research\Thesis\Project\Build"
capture cd "C:\Users\Andrew\Dropbox\Research\Thesis\Project\Build"

*premerge dta conversion
foreach i of numlist 1996 1999 2000 2003 2006 2010 {
	display(`i')
	insheet using `"Input\lights`i'.csv"', clear
	drop objectid area
	ren objectid_1 objectid
		
	ren min light_min_`i'
	label var light_min_`i'
	ren max light_max_`i'
	label var light_max_`i'
	ren range light_range_`i'
	label var light_range_`i'
	ren mean light_mean_`i'
	label var light_mean_`i'
	ren std light_std_`i'
	label var light_std_`i'
	ren sum light_`i'
	label var light_`i'
	gen light_obs_`i' = light_`i' / light_mean_`i'
	
	save `"Input\lights`i'.dta"', replace
}

*population data
insheet using `"Input\pop2000.csv"', clear
drop objectid area count
ren objectid_1 objectid
ren sum pop_2000
save `"Input\pop2000.dta"', replace

insheet using "Input\pop2010.csv", clear
drop objectid area count
ren objectid_1 objectid
ren sum pop_2010
save "Input\pop2010.dta", replace

*dmspols data
insheet using `"Input\dmspols1999_f12.csv"', clear
keep objectid_1 sum max min
ren objectid_1 objectid
ren sum dmsp_1999_f12
ren max dmsp_max_1999_f12
ren min dmsp_min_1999_f12
save `"Input\dmspols1999_f12.dta"', replace

insheet using `"Input\dmspols1999_f14.csv"', clear
keep objectid_1 sum max min
ren objectid_1 objectid
ren sum dmsp_1999_f14
ren max dmsp_max_1999_f14
ren min dmsp_min_1999_f14
save `"Input\dmspols1999_f14.dta"', replace


insheet using "Input\dmspols2000_f14.csv", clear
keep objectid_1 sum max min
ren objectid_1 objectid
ren sum dmsp_2000_f14
ren max dmsp_max_2000_f14
ren min dmsp_min_2000_f14
save "Input\dmspols2000_f14.dta", replace

insheet using "Input\dmspols2000_f15.csv", clear
keep objectid_1 sum max min
ren objectid_1 objectid
ren sum dmsp_2000_f15
ren max dmsp_max_2000_f15
ren min dmsp_min_2000_f15
save "Input\dmspols2000_f15.dta", replace

insheet using `"Input\dmspols2009_f16.csv"', clear
keep objectid_1 sum max min
ren objectid_1 objectid
ren sum dmsp_2009_f16
ren max dmsp_max_2009_f16
ren min dmsp_min_2009_f16
save `"Input\dmspols2009_f16.dta"', replace

insheet using "Input\dmspols2010_f18.csv", clear
keep objectid_1 sum max min
ren objectid_1 objectid
ren sum dmsp_2010_f18
ren max dmsp_max_2010_f18
ren min dmsp_min_2010_f18
save "Input\dmspols2010_f18.dta", replace

*cropland and pasture data
insheet using `"Input\cropland.csv"', clear
keep objectid_1 mean
ren objectid_1 objectid
ren mean cropland
save `"Input\cropland.dta"', replace

insheet using "Input\pasture.csv", clear
keep objectid_1 mean
ren objectid_1 objectid
ren mean pastureland
save "Input\pasture.dta", replace

*main merge
import delimited "Input\regions.csv", encoding(UTF-8) clear
ren area2 area

foreach i of numlist 1996 1999 2000 2003 2006 2010 {
	merge 1:1 objectid using `"Input\lights`i'.dta"'
	drop _merge
}

merge 1:1 objectid using "Input\pop2000.dta"
drop _merge
merge 1:1 objectid using "Input\pop2010.dta"
drop _merge

merge 1:1 objectid using "Input\dmspols1999_f12.dta"
drop _merge
merge 1:1 objectid using "Input\dmspols1999_f14.dta"
drop _merge
merge 1:1 objectid using "Input\dmspols2000_f14.dta"
drop _merge
merge 1:1 objectid using "Input\dmspols2000_f15.dta"
drop _merge
merge 1:1 objectid using "Input\dmspols2009_f16.dta"
drop _merge
merge 1:1 objectid using "Input\dmspols2010_f18.dta"
drop _merge

merge 1:1 objectid using "Input\cropland.dta"
drop _merge
merge 1:1 objectid using "Input\pasture.dta"
drop _merge

*regionid to merge with other data
sort region
sort country
gen temp = trim(region) + ", " + trim(country)
*egen regionid = sieve(temp), keep(a n space)
gen regionid = temp
drop temp

save "Output/regionlights.dta", replace
********************************************************************************
import excel "Input\regionaldata.xlsx", sheet("Sheet1") firstrow clear

drop if year < 1996

sort Region
sort Country

gen temp = trim(Region) + ", " + trim(Country)
*egen regionid = sieve(temp), keep(a n space)
gen regionid = temp
drop temp

replace Code = "DEU" if Code == "DDR" | Code == "BRD"
drop Regionallights LightsCountry LnPopulationdensity

reshape wide LnCumOilGasProd GDPpcRegion GDPpcCountry YearsofEducation, i(regionid) j(year)

merge 1:1 regionid using "Output/regionlights.dta"
drop if _merge != 3
drop _merge

drop Latitude
*correction to population data
drop if region == "Calarasi"

gen dmsp_1999 = (dmsp_1999_f12 + dmsp_1999_f14)/2
gen dmsp_2000 = (dmsp_2000_f14 + dmsp_2000_f15)/2
gen dmsp_2009 = dmsp_2009_f16
gen dmsp_2010 = dmsp_2010_f18

gen dmsp_9900 = (dmsp_1999 + dmsp_2000)/2
gen dmsp_0910 = (dmsp_2009 + dmsp_2010)/2
gen l_dmsp_diff = ln(dmsp_0910/dmsp_9900)

*interannual calibration to 2006 base year, radiance calibrated
replace light_1996 = 4.336*light_obs_1996 + 0.915*light_1996
replace light_1999 = 1.423*light_obs_1999 + 0.780*light_1999
replace light_2000 = 3.658*light_obs_2000 + 0.710*light_2000
replace light_2003 = 3.736*light_obs_2003 + 0.797*light_2003
replace light_2010 = 2.196*light_obs_2010 + 1.195*light_2010

foreach i of numlist 1996 1999 2000 2003 2006 2010 {
	gen light_area_`i' = light_`i'/area
	capture gen l_GDPpcRegion`i' = ln(GDPpcRegion`i')
	capture gen l_GDPpcCountry`i' = ln(GDPpcCountry`i')
}

gen GDPRegion2000 = GDPpcRegion2000*pop_2000
gen GDPRegion2010 = GDPpcRegion2010*pop_2010

egen GDPCountry2000 = total(GDPRegion2000), by(country)
egen GDPCountry2010 = total(GDPRegion2010), by(country)

egen area_country = total(area), by(country)

********************************************************************************
*****************experimental***************************************************
********************************************************************************
gen GDP_share_2000 = GDPRegion2000/GDPCountry2000
gen GDP_share_2010 = GDPRegion2010/GDPCountry2010

egen light_country_2000 = total(light_2000), by(country)
gen light_share_2000 = light_2000/light_country_2000
egen light_country_2010 = total(light_2010), by(country)
gen light_share_2010 = light_2010/light_country_2010

gen light_int_ratio_2000 = (light_2000/light_country_2000)/area

gen cGDP_area_2000 = GDPCountry2000/area_country
gen rGDP_area_2000 = GDPRegion2000/area

gen cGDP_area_2010 = GDPCountry2010/area_country
gen rGDP_area_2010 = GDPRegion2010/area
********************************************************************************
********************************************************************************
********************************************************************************

gen light_pc_2000 = light_2000/pop_2000
gen light_pc_2010 = light_2010/pop_2010

gen l_GDPRegion2000 = ln(GDPRegion2000)
gen l_GDPRegion2010 = ln(GDPRegion2010)
gen l_GDPpcRegion_diff = l_GDPpcRegion2010 - l_GDPpcRegion2000
gen l_GDPRegion_diff = l_GDPRegion2010 - l_GDPRegion2000

gen l_light_pc_2000 = ln(light_pc_2000)
gen l_light_pc_2010 = ln(light_pc_2010)
gen l_light_2000 = ln(light_2000)
gen l_light_2010 = ln(light_2010)
gen l_light_pc_diff = l_light_pc_2010 - l_light_pc_2000
gen l_light_diff = l_light_2010 - l_light_2000
gen l2_light_pc_diff = l_light_pc_diff^2
gen l2_light_diff = l_light_diff^2
gen l3_light_diff = l_light_diff^3

gen l_dmsp_pc_1999 = ln(dmsp_1999/pop_2000)
gen l_dmsp_pc_2009 = ln(dmsp_2009/pop_2010)
gen l_dmsp_pc_2000 = ln(dmsp_2000/pop_2000)
gen l_dmsp_pc_2010 = ln(dmsp_2010/pop_2010)

gen l_dmsp_1999 = ln(dmsp_1999)
gen l_dmsp_2009 = ln(dmsp_2009)
gen l_dmsp_2000 = ln(dmsp_2000)
gen l_dmsp_2010 = ln(dmsp_2010)

gen l_dmsp_pc_diff_00 = l_dmsp_pc_2010 - l_dmsp_pc_2000
gen l_dmsp_pc_diff_99 = l_dmsp_pc_2009 - l_dmsp_pc_1999
gen l_dmsp_diff_99 = l_dmsp_2009 - l_dmsp_1999
gen l_dmsp_diff_00 = l_dmsp_2010 - l_dmsp_2000

gen l2_dmsp_pc_diff_99 = l_dmsp_pc_diff_99^2
gen l2_dmsp_diff_99 = l_dmsp_diff_99^2
gen l2_dmsp_pc_diff_00 = l_dmsp_pc_diff_00^2
gen l2_dmsp_diff_00 = l_dmsp_diff_00^2

gen popdens2000 = pop_2000/area
gen popdens2010 = pop_2010/area
gen l_popdens_diff = ln(popdens2010)-ln(popdens2000)
gen l_pop_diff = ln(pop_2010)-ln(pop_2000)

gen l_area = ln(area)

//drop if GDPpcRegion1996 == . & GDPpcRegion1997 == . & GDPpcRegion1998 == . & GDPpcRegion1999 == . & GDPpcRegion2000 == . & GDPpcRegion2001 == . & GDPpcRegion2002 == . & GDPpcRegion2003 == . & GDPpcRegion2004 == . & GDPpcRegion2005 == . & GDPpcRegion2006 == . & GDPpcRegion2007 == . & GDPpcRegion2008 == . & GDPpcRegion2009 == . & GDPpcRegion2010 == .

ren (l_GDPpcRegion_diff l_GDPRegion_diff) (l_rGDP_pc_diff l_rGDP_diff)
ren (popdens2000 popdens2010 l_GDPpcRegion2000 l_GDPpcRegion2010 l_GDPRegion2000 l_GDPRegion2010) (popdens_2000 popdens_2010 l_rGDP_pc_2000 l_rGDP_pc_2010 l_rGDP_2000 l_rGDP_2010)
forvalues i = 1996/2010 {
capture ren LnCumOilGasProd`i' l_oilgas_`i'
capture ren YearsofEducation`i' yr_edu_`i' 
capture ren GDPpcCountry`i' cGDP_pc_`i'
capture ren GDPpcRegion`i' rGDP_pc_`i'
capture ren GDPCountry`i' cGDP_`i'
capture ren GDPRegion`i' rGDP_`i'
}

/*
label var l_rGDP_diff "Log diff. GDP"
label var l_light_diff "Log diff. lights"
label var l2_light_diff "Log diff. lights sq."
label var l3_light_diff "Log diff. lights cu."
label var l_pop_diff "Log diff. population"
label var l_rGDP_pc_diff "Log diff. GDP p.c."
label var l_light_pc_diff "Log diff. lights p.c."
*/

label var l_rGDP_diff "\$\Delta ln(GDP)\$"
label var l_light_diff "\$\Delta ln(Lights)\$"
label var l2_light_diff "\$\Delta ln(Lights)^2\$"
label var l3_light_diff "\$\Delta ln(Lights)^3\$"
label var l_pop_diff "\$\Delta ln(Pop.)\$"
label var l_rGDP_pc_diff "\$\Delta ln(GDP p.c.)\$"

label var l_dmsp_diff_99 "Log diff. non-calibrated lights 1999-2009"
label var l_dmsp_pc_diff_99 "Log diff. non-calibrated lights p.c. 1999-2009"
label var l_dmsp_diff_00 "Log diff. non-calibrated lights 2000-2010"
label var l_dmsp_pc_diff_00 "Log diff. non-calibrated lights p.c. 2000-2010"

label var cGDP_2000 "Country GDP"
label var cGDP_2010 "Country GDP"
label var rGDP_2000 "Region GDP"
label var rGDP_2010 "Region GDP"
label var cGDP_pc_2000 "Country GDP p.c."
label var cGDP_pc_2010 "Country GDP p.c."
label var rGDP_pc_2000 "Region GDP p.c."
label var rGDP_pc_2010 "Region GDP p.c."
label var popdens_2000 "Pop. density"
label var popdens_2010 "Pop. density"
label var cropland "% cropland"
label var pastureland "% pastureland"

sort l_light_diff
encode country, gen(countryid)

qui reg l_light_pc_diff i.countryid
predict l_light_pc_diff_net, re
qui reg l_rGDP_pc_diff i.countryid
predict l_rGDP_pc_diff_net, re

qui reg l_light_diff i.countryid
predict l_light_diff_net, re
qui reg l_rGDP_diff i.countryid
predict l_rGDP_diff_net, re

drop if region == ""

gen nuts = .
replace nuts = 4 if pop_2000 >= 7000000 //I+
replace nuts = 3 if pop_2000 >= 3000000 & pop_2000 < 7000000 //I
replace nuts = 2 if pop_2000 >= 800000 & pop_2000 < 3000000 //II
replace nuts = 1 if pop_2000 >= 150000 & pop_2000 < 800000 //III
replace nuts = 0 if pop_2000 < 150000 //III-

gen latitude_abs = abs(latitude)
label var latitude_abs "Abs. latitude"
label var CapitalisinRegion "Capital region"
label var l_area "Log area (\$km^2\$)"

save "Output/regionlights.dta", replace

********************************************************************************
********************************************************************************
cd "Input"

shp2dta using "Shape\regions2_83_onworldmapbasis.shp", database(regions2_83_onworldmapbasis) coordinates(coord) replace
use regions2_83_onworldmapbasis.dta
ren OBJECTID objectid

merge 1:1 objectid using "..\Output\regionlights.dta"
ren objectid OBJECTID
drop if _merge != 3

spmat contiguity mat_sw using coord.dta, id(_ID) norm(row) replace

spmat lag l_light_diff_sp mat_sw l_light_diff
sum l_light_diff l_light_diff_sp

/*
spmap l_light_diff using coord, id(_ID) mfc(blue) fc(RdYlBu) ndo(black) clb(-1 -.40 -0.30 -0.20 -0.10 0 0.10 0.20 0.30 0.40 1) clm(custom)
graph export ../../../Draft/Figures/lightsgrowthmap.png, replace
spmap l_rGDP_diff using coord, id(_ID) mfc(blue) fc(RdYlBu) ndo(black) clb(-1 -.40 -0.30 -0.20 -0.10 0 0.10 0.20 0.30 0.40 1) clm(custom)
graph export ../../../Draft/Figures/gdpgrowthmap.png, replace
*/

label var l_light_diff_sp "Spatial lag log diff. lights"
drop _merge

save "..\Output\regionlights.dta", replace
