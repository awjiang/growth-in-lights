*Figures

graph drop _all
capture log close
eststo clear
set more off
set graphics on

capture cd "C:\Users\awjia\Dropbox\Research\Thesis\Project"
capture cd "C:\Users\Andrew\Dropbox\Research\Thesis\Project"
capture mkdir "Analyse\Output\Figures"
capture mkdir "Analyse\Output\Tables"

use "Build\Output\RegionLights.dta", clear

*drop countries with no GDP data in 2000, 2010; drop countries with just one region
drop if l_rGDP_pc_diff == .
bysort country: drop if _N == 1

//raw data plots

*cd "Analyse\Output\Figures"
cd "..\Draft\Figures"

/*GDP
twoway (scatter l_rGDP_pc_2010 l_rGDP_pc_2000, msize(tiny) xtitle("Log Regional GDP per Capita 2000") ytitle("Log Regional GDP per Capita 2010")) (lfit l_rGDP_pc_2000 l_rGDP_pc_2000, legend(label(2 "Unit Line")))
graph export "rGDP_l_pc.png", replace
twoway (scatter rGDP_pc_2010 rGDP_pc_2000, msize(tiny) xtitle("Regional GDP per Capita 2000") ytitle("Regional GDP per Capita 2010")) (lfit rGDP_pc_2000 rGDP_pc_2000, legend(label(2 "Unit Line")))
graph export "rGDP_pc.png", replace
twoway (scatter l_rGDP_2010 l_rGDP_2000, msize(tiny) xtitle("Log Regional GDP 2000") ytitle("Log Regional GDP 2010")) (lfit l_rGDP_2000 l_rGDP_2000, legend(label(2 "Unit Line")))
graph export "rGDP_l.png", replace
*Light
twoway (scatter l_light_2010 l_light_2000, msize(tiny) xtitle("Regional Lights per Capita 2000") ytitle("Regional Lights per Capita 2010")) (lfit l_light_2000 l_light_2000, legend(label(2 "Unit Line")))
graph export "LightsRegion_l.png", replace
twoway (scatter l_light_pc_2010 l_light_pc_2000, msize(tiny) xtitle("Log Regional Lights per Capita 2000") ytitle("Log Regional Lights per Capita 2010")) (lfit l_light_pc_2000 l_light_pc_2000, legend(label(2 "Unit Line")))
graph export "LightsRegion_l_pc.png", replace
*/
//replace pop_2000 = round(pop_2000)
//loc weight [fweight=pop_2000]

*Growth densities
kdens l_light_diff `weight', kern(gau) nograph
loc light_bw: di %3.2f `= r(width)'
kdens l_rGDP_pc_diff `weight', kern(gau) nograph
loc rGDP_pc_bw: di %3.2f `= r(width)'
kdens l_rGDP_diff `weight', kern(gau) nograph
loc rGDP_bw: di %3.2f `= r(width)'
kdens l_pop_diff `weight', kern(gau) nograph
loc pop_bw: di %3.2f `= r(width)'

twoway (kdens l_light_diff, kern(gau)) (kdens l_rGDP_pc_diff, kern(gau)) ///
(kdens l_rGDP_diff, kern(gau)) (kdens l_pop_diff, kern(gau)), xtitle("") ///
legend(label(1 `"Log diff. lights, bw = `light_bw'"') label(2 `"Log diff. GDP p.c., bw = `rGDP_pc_bw'"') ///
label(3 `"Log diff. GDP, bw = `rGDP_pc_bw'"') label(4 `"Log diff. pop., bw = `pop_bw'"'))
graph export "kdens.png", replace

loc bw = 0.8
twoway (scatter l_rGDP_diff l_light_diff) (lowess l_rGDP_diff l_light_diff, bwidth(`bw')) (qfit l_rGDP_diff l_light_diff), ///
xtitle("Log diff. lights") ytitle("Log diff. GDP") legend(label(2 `"Lowess, bandwidth = `bw'"') label(3 "Quadratic fit") rows(1))
graph export "GDP_lights_noFE.png", replace
twoway (scatter l_rGDP_diff_net l_light_diff_net) (lowess l_rGDP_diff_net l_light_diff_net, bwidth(`bw')) ///
(lfit l_rGDP_diff_net l_light_diff_net), xtitle("Log diff. lights FE") ytitle("Log diff. GDP FE") legend(label(2 `"Lowess, bandwidth = `bw'"') label(3 "Linear fit") rows(1))
graph export "GDP_lights_FE.png", replace

**Appendix
preserve
scalar cutoff = 100000
//Original IV
*growth
twoway (scatter l_light_diff l_dmsp_diff_00, msize(small)) (line l_light_diff l_light_diff), xtitle(Log diff. DMSP) ytitle(Log diff. lights) legend(off)
graph export "lights_dmsp_growth.png", replace

*levels breakdown
scatter l_light_2000 l_dmsp_2000, msize(small) xtitle(Log DMSP) ytitle(Log lights) legend(off)
graph export "lights_dmsp_2000.png", replace
scatter l_light_2010 l_dmsp_2010, msize(small) xtitle(Log DMSP) ytitle(Log lights) legend(off)
graph export "lights_dmsp_2010.png", replace

keep if dmsp_2000 > cutoff
*growth
twoway (scatter l_light_diff l_dmsp_diff_00, msize(small)) (line l_light_diff l_light_diff), xtitle(Log diff. DMSP) ytitle(Log diff. lights) legend(off)
graph export "lights_dmsp_growth_cut.png", replace
*levels breakdown
scatter l_light_2000 l_dmsp_2000, msize(small) xtitle(Log DMSP) ytitle(Log lights) legend(off)
graph export "lights_dmsp_2000_cut.png", replace
scatter l_light_2010 l_dmsp_2010, msize(small) xtitle(Log DMSP) ytitle(Log lights) legend(off)
graph export "lights_dmsp_2010_cut.png", replace
restore

****quad pic no FE
loc agglom 0.nuts 1.nuts 2.nuts 3.nuts 4.nuts
loc control `"l_area CapitalisinRegion latitude_abs `agglom'"'

xtile pct4_cGDP_pc_2000 = cGDP_pc_2000, nq(4)
reg l_rGDP_diff l_light_diff l2_light_diff c.l_light_diff#pct4_cGDP_pc_2000 c.l2_light_diff#pct4_cGDP_pc_2000 i.pct4_cGDP_pc_2000 `control'
predict quad_xb, xb
reg l_rGDP_diff i.pct4_cGDP_pc_2000 `control'
predict ctrl_xb, xb

twoway (scatter l_rGDP_diff l_light_diff) (scatter quad_xb l_light_diff, msize(small)) (scatter ctrl_xb l_light_diff, msize(small)), xtitle(Log diff. lights) ///
legend(col(1) label(1 "Log diff. GDP") label(2 "Quadratic with standard controls and country GDP p.c. quartile indicators") label(3 "Standard controls and country GDP p.c. quartile indicators"))
graph export "quadfit.png", replace
