capture log close
eststo clear
set more off
set graphics on

capture cd "C:\Users\awjia\Dropbox\Research\Thesis\Project"
capture cd "C:\Users\Andrew\Dropbox\Research\Thesis\Project"

use "Build\Output\RegionLights.dta", clear

*drop countries with no GDP data in 2000, 2010; drop countries with just one region
drop if l_rGDP_pc_diff == .
bysort country: drop if _N == 1

********************************************************************************
********************************************************************************
********************************************************************************
gen aic_graph = .
gen bic_graph = .
gen graph_x = .

loc deg 10
foreach options in "vce(robust)" "vce(robust) abs(country)" {
	if `"`options'"' == "vce(robust)" {
		loc fe
	}
	
	if `"`options'"' == "vce(robust) abs(country)" {
		loc fe fe
	}
	
	replace aic_graph = .
	replace bic_graph = .
	replace graph_x = .

	mat IC = J(`deg', 3, .)
	loc reg qui reg l_rGDP_diff l_light_diff
	`reg', `options'
	mat IC[1,3] = e(r2)
	estat ovtest
	return list
	
	qui estat ic
	mat S = r(S)
	loc aic = S[1,5]
	loc bic = S[1,6]
	mat IC[1,1] = `aic'
	mat IC[1,2] = `bic'

	replace aic_graph = `aic' if _n == 1
	replace bic_graph = `bic' if _n == 1
	replace graph_x = 1 if _n == 1

	forvalues i = 2/`deg' {
		capture gen l`i'_light_diff = l_light_diff^`i'
		loc reg `reg' l`i'_light_diff
		`reg', `options'
		mat IC[`i',3] = e(r2)
		qui estat ic
		mat S = r(S)
		loc aic = S[1,5]
		loc bic = S[1,6]
		mat IC[`i',1] = `aic'
		mat IC[`i',2] = `bic'
		
		replace aic_graph = `aic' if _n == `i'
		replace bic_graph = `bic' if _n == `i'
		replace graph_x = `i' if _n == `i'
	}

	matlist IC
	
	sort graph_x
	twoway (line aic_graph graph_x) (line bic_graph graph_x), legend(label(1 "AIC") label(2 "BIC")) xtitle("Polynomial model degree") ytitle("IC")
	graph export `"..\Draft\Figures\ic`fe'.png"', replace
	
}
