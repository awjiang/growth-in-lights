*Tables

capture log close
eststo clear
set more off
set graphics on

capture cd "C:\Users\awjia\Dropbox\Research\Thesis\Project"
capture cd "C:\Users\Andrew\Dropbox\Research\Thesis\Project"
capture mkdir "Analyse\Output\Figures"
capture mkdir "Analyse\Output\Tables"

use "Build\Output\RegionLights.dta", clear

*drop countries with no GDP data in 2000, 2010; drop countries with just one region
drop if l_rGDP_pc_diff == .
bysort country: drop if _N == 1

loc agglom 0.nuts 1.nuts 2.nuts 3.nuts 4.nuts
loc control `"l_area CapitalisinRegion latitude_abs `agglom'"'

foreach var in `control' yr_edu_2000 cropland {
	drop if `var' == .
	}

drop countryid
sort country
*encode country, gen(countryid)	
egen countryid = group(country)
	
qui tab country
loc countrycount = r(r)
loc countryid
forvalues i = 1/`countrycount'{
	loc countryid `countryid' `i'.countryid
	}

loc opt_noFE , vce(cl country)
loc opt_FE `countryid', vce(cl country)
	
//cd "Analyse\Output\Tables"

cd "..\Draft\Tables"
********************************************************************************
**************************Quartile Charts***************************************
********************************************************************************
loc k = 0
mat q = J(4, 3, .)
mat coln q = "Country GDP p.c." "Region GDP p.c." "Lights share"
mat rown q = "Q1" "Q2" "Q3" "Q4"
foreach j of varlist cGDP_pc_2000 rGDP_pc_2000 light_share_2000{
	xtile pct4_`j' = `j', nq(4)
	loc k = `k' + 1
		forvalues i = 1/4 {
			sum `j' if pct4_`j' == `i'
			mat q[`i', `k'] = r(min)
		}
	}

esttab matrix(q, fmt("0 0 0 0" "0 0 0 0" "3 3 3 3"))using "..\Tables\q_cutoffs_GDP.tex", tex align(cc) nomtitle replace addnote("GDP cutoffs in USD (2014 PPP))") f substitute("\hline" "\toprule")

loc k = 0
mat q = J(4, 3, .)
mat coln q = "Pop. per $ km^2$" "\% Cropland" "Years Educ."
mat rown q = "Q1" "Q2" "Q3" "Q4"
foreach j of varlist popdens_2000 cropland yr_edu_2000 {
	xtile pct4_`j' = `j', nq(4)
	loc k = `k' + 1
		forvalues i = 1/4 {
			sum `j' if pct4_`j' == `i'
			mat q[`i', `k'] = r(min)
		}
	}

esttab matrix(q, fmt(3))using "..\Tables\q_cutoffs_other.tex", tex align(cc) nomtitle replace f substitute("toprule" "hline" "midrule" "hline" "bottomrule" "hline")

********************************************************************************
*************************Baseline***********************************************
********************************************************************************

foreach j in "" "_c" {
	loc c ""
	if "`j'" == "_c" {
		loc c `control'
		}

	eststo: qui reg l_rGDP_diff l_light_diff `c' `opt_noFE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	eststo: qui reg l_rGDP_diff l_light_diff l2_light_diff `c' `opt_noFE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	eststo: qui reg l_rGDP_diff l_light_diff l2_light_diff l3_light_diff `c' `opt_noFE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	eststo: qui reg l_rGDP_diff l_light_diff `c' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	eststo: qui reg l_rGDP_diff l_light_diff l2_light_diff `c' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	
	if "`j'" == "_c" {
		esttab using baseline`j'_v2.tex, replace label compress se booktabs nonote drop(`countryid' 0.nuts) ///
		mtitles("Linear" "Quad" "Cubic" "Linear FE" "Quad FE") width(\hsize) noomit order(l_light_diff l2_light_diff l3_light_diff `c') ///
		stats(N r2 r2_a aic, labels("Observations" "\$R^2\$" "Adj. \$R^2\$" "AICc")) varlabels(_cons Constant 1.nuts "NUTS III" 2.nuts "NUTS II" 3.nuts "NUTS I" 4.nuts "\textgreater NUTS I")
		}
	if "`j'" == "" {
		esttab using baseline`j'_v2.tex, replace label compress se booktabs nonote drop(`countryid') ///
		mtitles("Linear" "Quad" "Cubic" "Linear FE" "Quad FE") width(\hsize) noomit order(l_light_diff l2_light_diff l3_light_diff `c') ///
		stats(N r2 r2_a aic, labels("Observations" "\$R^2\$" "Adj. \$R^2\$" "AICc")) varlabels(_cons Constant 1.nuts "NUTS III" 2.nuts "NUTS II" 3.nuts "NUTS I" 4.nuts "\textgreater NUTS I")
		}	
	eststo clear
	}
	
********************************************************************************
****************************Poly, FE, and Decomp Regressions********************
********************************************************************************
loc control_ind `control' i.pct4_cGDP_pc_2000
******************************************POLY
capture drop Q
foreach depvar of varlist l_rGDP_diff l_rGDP_pc_diff l_pop_diff {
	
	*baseline
	eststo r1_noFE_`depvar': qui reg `depvar' c.l_light_diff c.l2_light_diff `control' `opt_noFE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	eststo r1_noFE_`depvar'_ind: reg `depvar' l_light_diff l2_light_diff `control_ind' `opt_noFE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	
	*interaction
	loc j = 2
	foreach qvar of varlist cGDP_pc_2000 light_share_2000 cropland {
		xtile Q = `qvar', nq(4)
		eststo r`j'_noFE_`depvar': qui reg `depvar' c.l_light_diff c.l2_light_diff c.l_light_diff#Q c.l2_light_diff#Q i.Q `control_ind' `opt_noFE'
			estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
		drop Q
		loc j = `j' + 1
	}
}


********************************************************************************

eststo r2_noFE_l_rGDP_diff_ind: reg l_rGDP_diff l_light_diff l2_light_diff c.l_light_diff#pct4_cGDP_pc_2000 c.l2_light_diff#pct4_cGDP_pc_2000 `control_ind' `opt_noFE'
	estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
loc stats stats(N r2 r2_a aicc, labels("Observations" "R^2" "Adj. R^2" "AICc") fmt(0 3 3 1))	

esttab r1_noFE_l_rGDP_diff r1_noFE_l_rGDP_diff_ind r2_noFE_l_rGDP_diff_ind using poly_1_noFE_v2.tex, replace compress booktabs nonote gap ///
cells(b(star fmt(3)) se(par fmt(3))) noomit `stats' drop(`control' 1.pct4_cGDP_pc_2000) /// 1.pct4_cGDP_pc_2000#c.l_light_diff) ///
order(l_light_diff 2.pct4_cGDP_pc_2000#c.l_light_diff 3.pct4_cGDP_pc_2000#c.l_light_diff 4.pct4_cGDP_pc_2000#c.l_light_diff ///
l2_light_diff 2.pct4_cGDP_pc_2000#c.l2_light_diff 3.pct4_cGDP_pc_2000#c.l2_light_diff 4.pct4_cGDP_pc_2000#c.l2_light_diff ///
_cons 2.pct4_cGDP_pc_2000 3.pct4_cGDP_pc_2000 4.pct4_cGDP_pc_2000) ///
label coef("2.pct4_cGDP_pc_2000#c.l_light_diff" "\quad X Country GDP p.c. Q2" "3.pct4_cGDP_pc_2000#c.l_light_diff" "\quad X Country GDP p.c. Q3" "4.pct4_cGDP_pc_2000#c.l_light_diff" "\quad X Country GDP p.c. Q4" ///
"2.pct4_cGDP_pc_2000#c.l2_light_diff" "\quad X Country GDP p.c. Q2" "3.pct4_cGDP_pc_2000#c.l2_light_diff" "\quad X Country GDP p.c. Q3" "4.pct4_cGDP_pc_2000#c.l2_light_diff" "\quad X Country GDP p.c. Q4" /// 
"2.pct4_cGDP_pc_2000" "\quad Country GDP p.c. Q2" "3.pct4_cGDP_pc_2000" "\quad Country GDP p.c. Q3" "4.pct4_cGDP_pc_2000" "\quad Country GDP p.c. Q4") ///
ml("Baseline" "Country GDP p.c." "Country GDP p.c.") collabels(none) num style(tex) ///
substitute("R^2" "\$R^2\$" ">" "\$>\$" "4 quantiles of cGDP_pc_2000=" "Country GDP p.c. Q" " X " "\$\times\$")

esttab r1_noFE_l_rGDP_diff_ind r3_noFE_l_rGDP_diff r4_noFE_l_rGDP_diff using poly_2_noFE_v2.tex, replace compress booktabs nonote gap ///
cells(b(star fmt(3)) se(par fmt(3))) noomit `stats' drop(`control' 1.pct4_cGDP_pc_2000 2.pct4_cGDP_pc_2000 3.pct4_cGDP_pc_2000 4.pct4_cGDP_pc_2000 1.Q) ///
label order(l_light_diff 2.Q#c.l_light_diff 3.Q#c.l_light_diff 4.Q#c.l_light_diff ///
l2_light_diff 2.Q#c.l2_light_diff 3.Q#c.l2_light_diff 4.Q#c.l2_light_diff ///
_cons 2.Q 3.Q 4.Q) ///
coef("2.Q#c.l_light_diff" "\quad X Q2" "3.Q#c.l_light_diff" "\quad X Q3" "4.Q#c.l_light_diff" "\quad X Q4" ///
"2.Q#c.l2_light_diff" "\quad X Q2" "3.Q#c.l2_light_diff" "\quad X Q3" "4.Q#c.l2_light_diff" "\quad X Q4" /// 
"2.Q" "\quad Q2" "3.Q" "\quad Q3" "4.Q" "\quad Q4") ///
ml("Baseline" "Lights share" "Cropland") collabels(none) num style(tex) ///
substitute("R^2" "\$R^2\$" ">" "\$>\$" " X " "\$\times\$")

loc control_ind `control' i.pct4_rGDP_pc_2000
***************************************FE
foreach depvar of varlist l_pop_diff l_rGDP_pc_diff l_rGDP_diff {
	
	*baseline d
	eststo r1_FE_`depvar': qui reg `depvar' l_light_diff `control' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	
	*gdpl
	xtile Q = cGDP_pc_2000, nq(4)
	eststo r2_FE_`depvar'_ind: qui reg `depvar' l_light_diff c.l_light_diff#Q `control_ind' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	drop Q

	xtile Q = rGDP_pc_2000, nq(4)
	eststo r1_FE_`depvar'_ind: qui reg `depvar' l_light_diff i.Q `control' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	*use with second FE table
	eststo r1_FE_`depvar'_ind2: qui reg `depvar' l_light_diff `control_ind' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	eststo rrgdp_FE_`depvar': qui reg `depvar' c.l_light_diff##Q `control' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	drop Q
	
	xtile Q = light_share_2000, nq(4)
	eststo r3_FE_`depvar'_ind: qui reg `depvar' c.l_light_diff##Q `control_ind' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	drop Q
	
	*other
	xtile Q = popdens_2000, nq(4)
	eststo rpop_FE_`depvar'_ind: qui reg `depvar' c.l_light_diff##Q `control_ind' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	drop Q	
	
	xtile Q = cropland, nq(4)
	eststo rcrop_FE_`depvar'_ind: qui reg `depvar' c.l_light_diff##Q `control_ind' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	drop Q
	
	xtile Q = yr_edu_2000, nq(4)
	eststo r4_FE_`depvar'_ind: qui reg `depvar' c.l_light_diff##Q `control_ind' `opt_FE'
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)
	drop Q
	
	}	
	
loc stats stats(N r2 r2_a aicc, labels("Observations" "R^2" "Adj. R^2" "AICc") fmt(0 3 3 1))	

esttab r1_FE_l_rGDP_diff r1_FE_l_rGDP_diff_ind rrgdp_FE_l_rGDP_diff using rgdp_FE.tex, replace compress booktabs nonote gap ///
se noomit `stats' keep(l_light_diff 2.Q#c.l_light_diff 3.Q#c.l_light_diff 4.Q#c.l_light_diff _cons 2.Q 3.Q 4.Q) ///
order(l_light_diff 2.Q#c.l_light_diff 3.Q#c.l_light_diff 4.Q#c.l_light_diff _cons 2.Q 3.Q 4.Q) ///
coef("2.Q#c.l_light_diff" "\quad X Regional GDP p.c. Q2" "3.Q#c.l_light_diff" "\quad X Regional GDP p.c. Q3" "4.Q#c.l_light_diff" "\quad X Regional GDP p.c. Q4" _cons "Constant" 2.Q "\quad Regional GDP p.c. Q2" 3.Q "\quad Regional GDP p.c. Q3" 4.Q "\quad Regional GDP p.c. Q4") ///
label ml("Baseline" "Regional GDP p.c." "Regional GDP p.c.") collabels(none) num style(tex) ///
substitute("R^2" "\$R^2\$" ">" "\$>\$" " X " "\$\times\$")

esttab r1_FE_l_rGDP_diff_ind2 r2_FE_l_rGDP_diff_ind r3_FE_l_rGDP_diff_ind rpop_FE_l_rGDP_diff_ind rcrop_FE_l_rGDP_diff_ind r4_FE_l_rGDP_diff_ind using other_FE.tex, replace compress booktabs nonote gap ///
se noomit `stats' keep(l_light_diff 2.Q#c.l_light_diff 3.Q#c.l_light_diff 4.Q#c.l_light_diff _cons 2.Q 3.Q 4.Q) ///
order(l_light_diff 2.Q#c.l_light_diff 3.Q#c.l_light_diff 4.Q#c.l_light_diff _cons 2.Q 3.Q 4.Q) ///
label ml("Baseline" "Country GDP p.c." "Lights share" "Pop. Density" "Cropland" "Years Educ.") collabels(none) num style(tex) ///
coef(2.Q#c.l_light_diff "\quad X Q2" 3.Q#c.l_light_diff "\quad X Q3" 4.Q#c.l_light_diff "\quad X Q4" _cons "Constant" 2.Q "\quad Q2" 3.Q "\quad Q3" 4.Q "\quad Q4") ///
substitute("R^2" "\$R^2\$" ">" "\$>\$" " X " "\$\times\$")

*decomp
loc stats stats(N r2 r2_a, labels("Observations" "R^2" "Adj. R^2") fmt(0 3 3))

esttab r1_FE_l_rGDP_diff_ind2 r1_FE_l_rGDP_pc_diff_ind2 r1_FE_l_pop_diff_ind2 using decomp_1_FE_v2.tex, replace compress booktabs nonote gap ///
se noomit `stats' coef(_cons Constant 1.nuts "NUTS III" 2.nuts "NUTS II" 3.nuts "NUTS I" 4.nuts "\textgreater NUTS I") ///
label ml("GDP" "GDP per capita" "Population") collabels(none) num style(tex) drop(`countryid' 0.nuts 1.pct4_rGDP_pc_2000 2.pct4_rGDP_pc_2000 3.pct4_rGDP_pc_2000 4.pct4_rGDP_pc_2000) ///
substitute("R^2" "\$R^2\$" ">" "\$>\$" " X " "\$\times\$")

esttab r2_FE_l_rGDP_diff_ind r2_FE_l_rGDP_pc_diff_ind r2_FE_l_pop_diff_ind using decomp_2_FE_v2.tex, replace compress booktabs nonote gap ///
se noomit `stats' coef(_cons Constant 1.nuts "NUTS III" 2.nuts "NUTS II" 3.nuts "NUTS I" 4.nuts "\textgreater NUTS I" /// 
"2.Q#c.l_light_diff" "\quad X Q2" "3.Q#c.l_light_diff" "\quad X Q3" "4.Q#c.l_light_diff" "\quad X Q4" /// 
"2.Q" "\quad Q2" "3.Q" "\quad Q3" "4.Q" "\quad Q4") drop(`countryid' 0.nuts 1.pct4_rGDP_pc_2000 2.pct4_rGDP_pc_2000 3.pct4_rGDP_pc_2000 4.pct4_rGDP_pc_2000) ///
label ml("GDP" "GDP per capita" "Population") collabels(none) num style(tex) ///
substitute("R^2" "\$R^2\$" ">" "\$>\$" " X " "\$\times\$")

eststo clear
********************************************************************************

eststo: reg l_rGDP_diff l_light_diff i.pct4_rGDP_pc_2000 `control' i.countryid, vce(cl country)
	estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)

eststo: reg l_rGDP_diff l_light_diff i.pct4_rGDP_pc_2000 l_light_diff_sp `control' i.countryid, vce(cl country)
	estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)

foreach Q in pct4_cropland pct4_yr_edu_2000 {
	capture drop Q
	gen Q = `Q'	
	eststo: reg l_rGDP_diff c.l_light_diff##Q i.pct4_rGDP_pc_2000 `control' i.countryid, vce(cl country)
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)

	eststo: reg l_rGDP_diff c.l_light_diff##Q i.pct4_rGDP_pc_2000 l_light_diff_sp `control' i.countryid, vce(cl country)
		estadd scalar aicc = 2*e(rank)-2*e(ll)+2*e(rank)*(e(rank)+1)/(e(N)-e(rank)-1)	
	}
	
esttab using spatial_FE.tex, replace compress booktabs nonote gap ///
se noomit stats(N r2 r2_a aicc, labels("Observations" "R^2" "Adj. R^2" "AICc") fmt(0 3 3 1)) ///
keep(l_light_diff 2.Q#c.l_light_diff 3.Q#c.l_light_diff 4.Q#c.l_light_diff _cons 2.Q 3.Q 4.Q l_light_diff_sp) ///
order(l_light_diff 2.Q#c.l_light_diff 3.Q#c.l_light_diff 4.Q#c.l_light_diff _cons 2.Q 3.Q 4.Q l_light_diff_sp) ///
label ml("Baseline" "Baseline Spatial" "Cropland" "Cropland Spatial" "Years Educ." "Years Educ. Spatial") collabels(none) num style(tex) ///
coef(2.Q#c.l_light_diff "\quad X Q2" 3.Q#c.l_light_diff "\quad X Q3" 4.Q#c.l_light_diff "\quad X Q4" _cons "Constant" 2.Q "\quad Q2" 3.Q "\quad Q3" 4.Q "\quad Q4") ///
substitute("R^2" "\$R^2\$" ">" "\$>\$" " X " "\$\times\$")
